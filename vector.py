'''This class is a tool to represent vectors.'''
from math import sqrt, acos, degrees, pi
from decimal import Decimal, getcontext

getcontext().prec = 30

class Vector(object):
    COORDINATES_EMPTY_MSG = 'Coordinates must be nonempty'
    CANNOT_NORMALIZE_ZERO_VECTOR_MSG = 'Cannot normalize the zero vector'
    NO_UNIQUE_PARALLEL_COMPONENT_MSG = 'Thhere is not a parallel component unique to the zero vector'
    NO_UNIQUE_ORTHOGONAL_COMPONENT_MSG = 'Thhere is not a orthogonal component unique to the zero vector'
    DIFFERENT_DIMENSIONS_MSG = 'The dimensions of the two vectos are different'
    DIFFERENT_CLASS_MSG = 'The class used in the sum is not compatible with the Vector class'
    INVALID_TYPE_SCALAR_MULT_MSG = 'Invalid type to perform Scalar Multiplication, please define a number'
    INVALID_DIMENSION_CROSS_PRODUCT_MSG = 'Invalid dimension to compute the cross product'

    def __init__(self, coordinates):
        try:
            if not coordinates:
                raise ValueError
            self.coordinates = tuple([Decimal(x) for x in  coordinates])
            self.dimension = len(coordinates)


        except Exception as e:
            if str(e) == self.COORDINATES_EMPTY_MSG:
                raise Exception(self.COORDINATES_EMPTY_MSG)
            else:
                raise e

    def __iter__(self):
        return iter(self.coordinates)

    def __getitem__(self, index):
        return self.coordinates[index]

    def format_answer(self, data, precision = 3):
        if type(data) is list:
            for i in range(len(data)):
                data[i] = round(data[i], precision)
        else:
            data = round(data, precision)
        return data

    '''Checks the two vectors are compatible with the operation requested'''
    def check_vector(self, v):
        try:
            if self.dimension != v.dimension:
                raise AttributeError
            elif v.__class__.__name__ != 'Vector':
                raise AttributeError

        except AttributeError:
            raise AttributeError(self.DIFFERENT_DIMENSIONS_MSG)

        except TypeError:
            raise TypeError(self.DIFFERENT_CLASS_MSG)

    '''Function that returns the default signature of the vector (Coordinates)'''
    def __str__(self):
        arr_str = list(round(self.coordinates[i], 3) for i in range(self.dimension))
        return 'Vector: {}'.format(arr_str)

    '''Rewrites how the function that compares if another vector is equal'''
    def __eq__(self, v):
        return self.coordinates == v.coordinates

    '''Define how the sum function works for two vectors'''
    def __add__(self, v):
           self.check_vector(v)
           coordinates = list(((self.coordinates[i] + v.coordinates[i]) for i in range(self.dimension)))
           coordinates = coordinates
           return Vector(coordinates)


    '''Define how the sub function works for two vectors'''
    def __sub__(self, v):
        self.check_vector(v)
        coordinates = list(map(lambda i: (self.coordinates[i] - v.coordinates[i]), range(self.dimension)))
        coordinates = coordinates
        return Vector(coordinates)

    '''Define how the multiplication works (Scalar Multiplication)'''
    def scalar_mult(self, s):
        s = Decimal(s)
        try:
            return Vector(list(map(lambda i: (self.coordinates[i] * s), range(self.dimension))))
        except ValueError:
            raise ValueError(self.INVALID_TYPE_SCALAR_MULT_MSG)

    '''Define how the magnutude is calculated'''
    def magnitude(self, to_show = False):
        # return (sum(list(map(lambda i: self.coordinates[i]**2, range(self.dimension)))))**(1/2)
        coordinates_squared = [x**2 for x in self.coordinates]
        out = Decimal(sqrt(sum(coordinates_squared)))
        if to_show:
            return self.format_answer(out)
        return out

    '''Define how the norm is calculated'''
    def normalized(self):
        try:
            magnitude = self.magnitude()
            # print(magnitude)
            return self.scalar_mult(1/magnitude)
        except ZeroDivisionError:
            raise Exception(self.CANNOT_NORMALIZE_ZERO_VECTOR_MSG)

        # magn = self.magnitude()
        # return list(map(lambda i: self.coordinates[i]*(1/magn), range(self.dimension)))

    def inner_product(self, v, to_show = False):
        self.check_vector(v)
        try:
            out = sum(list(map(lambda i: self.coordinates[i] * v.coordinates[i], range(self.dimension))))
            if to_show:
                return self.format_answer(out)
            return out
        except ZeroDivisionError:
            raise Exception(self.CANNOT_NORMALIZE_ZERO_VECTOR_MSG)

    def angle(self, v, in_degrees = False, to_show = False):
        self.check_vector(v)
        try:
            rads = acos(round(self.inner_product(v) / (self.magnitude() * v.magnitude()), 5))
            out = Decimal(rads)
            if in_degrees:
                out = (Decimal(180./pi)) * rads
            if to_show:
                return self.format_answer(out)
            return out
        except ZeroDivisionError:
            raise Exception(self.CANNOT_NORMALIZE_ZERO_VECTOR_MSG)

    def check_zero_vector(self, tolerance = 1e-3):
        return abs(self.magnitude()) < tolerance


    def check_orthogonal(self, v, tolerance = 1e-3):
        return abs(self.inner_product(v)) < tolerance

    def check_parallel(self, v, tolerance = 1e-3):
        self.check_vector(v)
        return self.check_zero_vector() or \
                v.check_zero_vector() or \
                self.angle(v) <= tolerance or \
                abs(self.angle(v) - Decimal(pi)) <= Decimal(tolerance)
        # try:
        #
        #     diff = self.coordinates[0] / v.coordinates[0]
        #     diff = self.format_answer(diff, 10)
        #     # print(diff)
        #     for i in range(1, self.dimension):
        #         aux_diff = self.format_answer(self.coordinates[i] / v.coordinates[i], 10)
        #         # print(aux_diff)
        #         if  aux_diff != diff:
        #             return False
        #     return True
        # except ValueError:
        #     raise ValueError('Not possible to check if vectos are parallel')


    def component_orthogonal_to(self, basis):
        try:
            projection = self.component_parallel_to(basis)
            return self - projection
        except Exception as e:
            if str(e) == self.NO_UNIQUE_PARALLEL_COMPONENT_MSG:
                raise Exception(self.NO_UNIQUE_ORTHOGONAL_COMPONENT_MSG)
            else:
                raise e

    def component_parallel_to(self, basis):
        try:
            u = basis.normalized()
            weight = self.inner_product(u)
            return u.scalar_mult(weight)
        except Exception as e:
            if str(e) == self.CANNOT_NORMALIZE_ZERO_VECTOR_MSG:
                raise Exception(self.NO_UNIQUE_PARALLEL_COMPONENT_MSG)
            else:
                raise e

    def check_dimension_cross_product(self):
        if self.dimension != 3:
            raise Exception(self.INVALID_DIMENSION_CROSS_PRODUCT_MSG)

    def cross_product(self, v):
        try:
            self.check_dimension_cross_product()
            v.check_dimension_cross_product()
            x_1, y_1, z_1 = self.coordinates
            x_2, y_2, z_2 = v.coordinates
            x = y_1 * z_2 - y_2 * z_1
            y = -(x_1 * z_2 - x_2 * z_1)
            z = x_1 * y_2 - x_2 * y_1
            return Vector([x, y, z])
        except Exception as e:
            raise e

    def area_of_parallelogram_with(self, v, to_show = False):
        cross = self.cross_product(v)
        out = cross.magnitude()
        if to_show:
            return self.format_answer(out)
        return out

    def area_of_triangle_with(self, v, to_show = False):
        out = self.area_of_parallelogram_with(v) / Decimal('2.0')
        if to_show:
            return self.format_answer(out)
        return out

    # TODO: Add testing

    # TODO: Make graphical example of vectors operations

def aux_paralel_orthogonal(v_1, v_2):
    print('Parallel: {}'.format(v_1.check_parallel(v_2)))
    print('Orthogonal: {}'.format(v_1.check_orthogonal(v_2)))
    print(' ')

def check_paralel_orthogonal():
    # #Verify Paralel and Orthogonal
    my_vector_1 = Vector([-7.579, -7.88])
    my_vector_2 = Vector([22.737, 23.64])
    aux_paralel_orthogonal(my_vector_1, my_vector_2)

    my_vector_3 = Vector([-2.029, 9.97, 4.172])
    my_vector_4 = Vector([-9.231, -6.639, -7.245])
    aux_paralel_orthogonal(my_vector_3, my_vector_4)

    my_vector_5 = Vector([-2.328, -7.284, -1.214])
    my_vector_6 = Vector([-1.821, 1.072, -2.94])
    aux_paralel_orthogonal(my_vector_5, my_vector_6)

    my_vector_7 = Vector([2.118, 4.827])
    my_vector_8 = Vector([0, 0])
    aux_paralel_orthogonal(my_vector_7, my_vector_8)

def check_inner_product():
    #Inner product (Inner Product is sum of multiplication of corresponding elements)

    my_vector = Vector([7.887, 4.138])
    my_vector_2 = Vector([-8.802, 6.776])
    print(my_vector.inner_product(my_vector_2))

    my_vector_3 = Vector([-5.955, -4.904, -1.874])
    my_vector_4 = Vector([-4.496, -8.755, 7.103])
    print(my_vector_3.inner_product(my_vector_4))

    my_vector_5 = Vector([3.183, -7.627])
    my_vector_6 = Vector([-2.668, 5.319])
    print(my_vector_5.angle(my_vector_6))

    my_vector_7 = Vector([7.35, 0.221, 5.188])
    my_vector_8 = Vector([2.751, 8.259, 3.985])
    print(my_vector_7.angle(my_vector_8, True))

def check_magnitude_direction():
    #Magnitude and Direction
    my_vector = Vector([-0.221, 7.437])
    my_vector_2 = Vector([8.813, -1.331, -6.247])
    my_vector_3 = Vector([5.581, -2.136])
    my_vector_4 = Vector([1.996, 3.108, -4.554])

    print(my_vector.magnitude())
    print(my_vector_2.magnitude())
    print(my_vector_3.normalized())
    print(my_vector_4.normalized())

def check_projecting():
    #Projection of b into V is proj b (v) or vII
    # = VII + VI, V = V paralel + V orghogonal
    vector_1 = Vector([3.039, 1.879])
    vector_2 = Vector([0.825, 2.036])
    print(vector_1.component_parallel_to(vector_2))

    vector_3 = Vector([-9.88, -3.264, -8.159])
    vector_4 = Vector([-2.155, -9.353, -9.473])
    print(vector_3.component_orthogonal_to(vector_4))

    vector_5 = Vector([3.009, -6.172, 3.692, -2.51])
    vector_6 = Vector([6.404, -9.144, 2.759, 8.718])
    print(vector_5.component_parallel_to(vector_6))
    print(vector_5.component_orthogonal_to(vector_6))

def check_cross_product():
    #Cross product of two parallel vactos e the 0 vector
    #cross product is orthogonal to both vectos

    vector_1 = Vector([8.462, 7.893, -8.187])
    vector_2 = Vector([6.984, -5.975, 4.778])
    print(vector_1.cross_product(vector_2))

    vector_3 = Vector([-8.987, -9.838, 5.031])
    vector_4 = Vector([-4.268, -1.861, -8.866])
    print(vector_3.area_of_parallelogram_with(vector_4))

    vector_5 = Vector([1.5, 9.547, 3.691])
    vector_6 = Vector([-6.007, 0.124, 5.772])
    print(vector_5.area_of_triangle_with(vector_6))




if __name__ == '__main__':

    # check_magnitude_direction()
    # check_projecting()
    check_cross_product()


