from plane import Plane
from decimal import Decimal
from vector import Vector

class Hyperplane(Plane):
    EITHER_DIM_OR_NORMAL_VEC_MUTS_BE_PROVIDED_MSG = 'Either dimension or normal vector of the hyperplane must be provided'

    def __init__(self, normal_vector, constant_term, dimension):
        if not dimension and not normal_vector:
            raise Exception(self.EITHER_DIM_OR_NORMAL_VEC_MUTS_BE_PROVIDED_MSG)
        super().__init__(normal_vector, constant_term, dimension)


