from line import Line
from vector import Vector

class Plane(Line):
    def __init__(self, normal_vector=None, constant_term=None, dimension = 3):
        super().__init__(normal_vector, constant_term, dimension)


def aux_test_plane(p1, p2):
    print('Plane 1 is parallel to Plane 2: {}'.format(p1.is_parallel_to(p2)))
    print('Plane 1 is equal to Plane 2: {}'.format(p1 == p2))
    print('')

def test_plane_parallel_equal():
    plane_1 = Plane(normal_vector=Vector([-0.412, 3.806, 0.728]), constant_term=-3.46)
    plane_2 = Plane(normal_vector=Vector([1.03, -9.515, -1.82]), constant_term=8.65)
    aux_test_plane(plane_1, plane_2)

    plane_1 = Plane(normal_vector=Vector([2.611, 5.528, 0.283]), constant_term=4.6)
    plane_2 = Plane(normal_vector=Vector([7.715, 8.306, 5.342]), constant_term=3.76)
    aux_test_plane(plane_1, plane_2)

    plane_1 = Plane(normal_vector=Vector([-7.926, 8.625, -7.217]), constant_term=-7.952)
    plane_2 = Plane(normal_vector=Vector([-2.642, 2.875, -2.404]), constant_term=-2.443)
    aux_test_plane(plane_1, plane_2)

def test_plane_intersection():

    pass

if __name__ == '__main__':
    test_plane_parallel_equal()

    # test_plane_intersection()